package com.panisa.week8;

public class CirclaApp {
    public static void main(String[] args) {
        Circle circle1 = new Circle(1);
        Circle circle2 = new Circle(2);
        circle1.printAreaCircle();
        circle1.printPerimeterCircle();
        circle2.printAreaCircle();
        circle2.printPerimeterCircle();
    }
    
}
