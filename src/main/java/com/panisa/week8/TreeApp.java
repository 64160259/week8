package com.panisa.week8;

public class TreeApp {
    public static void main(String[] args) {
        Tree tree1 = new Tree("tree1", 'T', 5, 10);
        tree1.print();
        Tree tree2 = new Tree("tree2", 't', 5, 11);
        tree2.print();
    }
}
