package com.panisa.week8;

public class Circle {
    // Attributes
    private double r;
    
    public Circle(double r) { // constructor
        this.r = r;
    }

    // Methodes
    public double printAreaCircle() {
        double area = (3.14)*r*r;
        System.out.println("Calculate area" + " = " + area);
        return area;
    }

    public double printPerimeterCircle() {
        double perimeter = 2*(3.14)*r;
        System.out.println("Calculate perimeter" + " = " + perimeter);
        return perimeter;
    }
}
