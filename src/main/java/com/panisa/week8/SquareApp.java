package com.panisa.week8;

public class SquareApp {
    public static void main(String[] args) {
        Square rect1 = new Square(10, 5);
        Square rect2 = new Square(5, 3);
        rect1.printAreaSquare();
        rect1.printPerimeterSquare();
        rect2.printAreaSquare();
        rect2.printPerimeterSquare();
    }   
}
