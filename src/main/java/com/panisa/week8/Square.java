package com.panisa.week8;

public class Square {
    // Attributes
    private double width;
    private double height;

    public Square(double width, double height) { // constructor
        this.width = width;
        this.height = height;
    }

    // Methodes
    public double printAreaSquare() {
        double area = width * height;
        System.out.println("Calculate area" + " = " + area);
        return area;
    }

    public double printPerimeterSquare() {
        double perimeter = (width * 2) + (height * 2);
        System.out.println("Calculate perimeter" + " = " + perimeter);
        return perimeter;
    }

    public void setWidth(double width) { // Setter Methods
        this.width = width;
    }

    public double getWidth() { //Getter Methods
        return width;
    }

    public double getHeight() {
        return height;
    }

}
