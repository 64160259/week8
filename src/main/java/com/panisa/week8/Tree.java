package com.panisa.week8;

public class Tree {
    // Attributes
    private String name;
    private char symbol;
    private int x;
    private int y;

    public Tree(String name,char symbol, int x, int y) {
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }

    public void print() {
        System.out.println(name + " " + "Symbol"+" = " + symbol + " " + "X"+" = " + x + " " + "Y"+" = " + y);
    }

    public void setName(String name) { // Setter Methods
        this.name = name;
    }

    public String getName() { // Getter Methods
        return name;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
